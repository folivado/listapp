package com.example.listapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.element_view.view.*


class MyAdapter (private  val myDataSet: Array<String>, private val listener: (String) -> Unit ):
        RecyclerView.Adapter<MyAdapter.MyViewHolder>() {

    class MyViewHolder(val elementsView: View): RecyclerView.ViewHolder(elementsView){
        private val itemVal = itemView.findViewById<TextView>(R.id.textView)

        fun bind(item: String){
            itemVal.text = item
        }
    }

    // Se crean nuevas vistas
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyAdapter.MyViewHolder {
        // creamos nueva vista
        val elementsView = LayoutInflater.from(parent.context).inflate(R.layout.element_view, parent, false)


        return  MyViewHolder(elementsView)
    }


    // Se reemplazan los campos de la vista
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // Obtiene el elemento de la posición dada
        // reemplaza el contenido de la vista con este elemento

        val item = myDataSet[position]
        holder.elementsView.textView.text = myDataSet[position]

        //holder.bind(myDataSet[position])

        holder.elementsView.setOnClickListener { listener(item) }
    }

    // Se devuelve el tamaño de myDataSet
    override fun getItemCount() = myDataSet.size

}