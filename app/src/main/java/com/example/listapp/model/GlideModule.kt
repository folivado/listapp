package com.example.listapp.model

import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.annotation.GlideModule

@GlideModule
class MyGlideModule : AppGlideModule()