package com.example.listapp.model

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url



interface APIService {

    @GET ("comments")
    fun getAllComments(): Call<List<Comments>>
}