package com.example.listapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.listapp.model.Comments
import kotlinx.android.synthetic.main.element_view.view.*


class MyAdapter2 (private  val myDataSet: MutableList<Comments>, private val listener: (Comments) -> Unit ):
        RecyclerView.Adapter<MyAdapter2.MyViewHolder>() {



    class MyViewHolder(val elementsView: View): RecyclerView.ViewHolder(elementsView){
        private val itemVal = itemView.findViewById<TextView>(R.id.textView)

        fun bind(item: Comments){
            itemVal.text = item.email
        }
    }



    // Se crean nuevas vistas
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyAdapter2.MyViewHolder {
        // creamos nueva vista
        val elementsView = LayoutInflater.from(parent.context).inflate(R.layout.element_view, parent, false)


        return  MyViewHolder(elementsView)
    }



    // Se reemplazan los campos de la vista
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // Obtiene el elemento de la posición dada
        // reemplaza el contenido de la vista con este elemento

        val item = myDataSet[position]

        holder.bind(item)

        holder.elementsView.setOnClickListener { listener(item) }
    }



    // Se devuelve el tamaño de myDataSet
    override fun getItemCount() = myDataSet.size



    fun addAll(list :List<Comments>){
        myDataSet.addAll(list)
        notifyDataSetChanged()
    }



    fun clear(){
        myDataSet.clear()
    }
}