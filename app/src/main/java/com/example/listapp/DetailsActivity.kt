package com.example.listapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        val textViewEmail = findViewById<TextView>(R.id.textViewEmail).apply {
            text = intent.getStringExtra(EMAIL_MESSAGE)
        }

        val textViewName = findViewById<TextView>(R.id.textViewName).apply {
            text = intent.getStringExtra(NAME_MESSAGE)
        }

        val textViewBody = findViewById<TextView>(R.id.textViewBody).apply {
            text = intent.getStringExtra(BODY_MESSAGE)
        }

        /*
        val imageViewPic = findViewById<TextView>(R.id.textViewBody).apply {
            contentDescription = intent.getStringExtra(IMAGE_DETAIL)
        }
        */
        Glide.with(this)
            .load("https://www.rover.com/blog/wp-content/uploads/2014/10/sad-dog-600x340.jpg")
            .into(imageViewPic)
    }
}