package com.example.listapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.annotation.UiThread
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.listapp.model.APIService
import com.example.listapp.model.Comments
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val EMAIL_MESSAGE = "com.example.listapp.EMAIL"
const val NAME_MESSAGE = "com.example.listapp.NAME"
const val BODY_MESSAGE = "com.example.listapp.BODY"
const val IMAGE_DETAIL = "com.example.listapp.IMAGE"

class MainActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: MyAdapter2
    private lateinit var viewManager: RecyclerView.LayoutManager

    //private var myDataSet2 : MutableList<Comments> = mutableListOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val res = searchComments()

        viewManager = LinearLayoutManager(this)
        viewAdapter = MyAdapter2(mutableListOf(), this::viewDetails)


        recyclerView = findViewById<RecyclerView>(R.id.my_recycler_view).apply {
            //setHasFixedSize(true)

            layoutManager = viewManager
            adapter = viewAdapter
        }
    }



    /*
        Pasamos los valores en el intent e iniciamos el activity para ver los detalles
     */
    fun viewDetails(cadena: Comments){
        val intent = Intent(this, DetailsActivity::class.java).apply{
            putExtra(EMAIL_MESSAGE, cadena.email)
            putExtra(NAME_MESSAGE, cadena.name)
            putExtra(BODY_MESSAGE, cadena.body)
            putExtra(IMAGE_DETAIL, cadena.email)
        }
        startActivity(intent)
    }

    private fun getRetrofit(): Retrofit{
        return Retrofit.Builder()
            .baseUrl("https://jsonplaceholder.typicode.com/")
            .client(OkHttpClient().newBuilder().addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            }).build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun searchComments() :Boolean{
        var resultado = false

        doAsync {
            Log.e("FIX", "Realizamos el intento de carga de datos")
            val call = getRetrofit().create(APIService::class.java).getAllComments().execute()

            val comments = call.body()


            comments?.let {

                if (call.isSuccessful) {
                    Log.e("FIX", "Carga de datos con éxito?")
                    uiThread {
                        viewAdapter.clear()
                        viewAdapter.addAll(comments)
                    }

                } else {
                    Log.e("FIX", "Carga de dato no realizada?")
                }
            }
            return@doAsync Unit
        }
        return resultado
    }
}